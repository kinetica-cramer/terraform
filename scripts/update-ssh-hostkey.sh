#!/bin/bash

check_installed() {
    prog=$(which $1)
    if [ -z "$prog" ]; then
        echo "$1 needs to be installed."
        exit 1
    fi
    echo $prog
}

nc=$(check_installed nc)
ssh_keygen=$(check_installed ssh-keygen)
ssh_keyscan=$(check_installed ssh-keyscan)

if [ -z "$1" ]; then
    echo "No host or ip given."
fi
for host in "$@"; do
    while ! ${nc} -z $host 22 &> /dev/null; do echo -n "."; done
    ${ssh_keygen} -R $host || /bin/true
    ${ssh_keyscan} $host >> ~/.ssh/known_hosts
done
