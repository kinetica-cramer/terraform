#!/usr/bin/env bash

device=/dev/xvdb

mkfs -t ext4 ${device}

cat /etc/fstab | grep -v ${device} > /tmp/fstab
echo "${device}     /srv    xfs     defaults 0 2" >> /tmp/fstab
mv /etc/fstab /etc/fstab.bak
mv /tmp/fstab /etc/fstab

mount -a