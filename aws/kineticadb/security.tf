variable "ssh_user" {
  description = "The user account to which to connect for provisioning"
}

variable "ssh_key_file" {
  description = "The location of the private key used to connect these nodes"
}

resource "aws_key_pair" "kineticadb" {
  key_name   = "${var.prefix}-gpudb"
  public_key = "${file(format("%s.pub", var.ssh_key_file))}"
}

output "ssh_user" {
  value = "${var.ssh_user}"
}

output "ssh_key_file" {
  value = "${var.ssh_key_file}"
}
