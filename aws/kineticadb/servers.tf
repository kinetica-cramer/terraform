variable "instance_count" {
  description = "The number of node instances for the confdiguration cluster"
  default     = 2
}

variable "instance_type" {
  description = "The amazon instance type of the configuration nodes"
  default     = "p2.xlarge"
}

variable "ami_id" {
  description = "The id of the AWS machine image to use for the configuration nodes"
}

variable "storage_type" {
  description = "The size in gigabytes of the additional (non-root) storage device"
  default     = "standard"
}

variable "storage_size" {
  description = "The size in gigabytes of the additional (non-root) storage device"
  default     = 400
}

variable "storage_iops" {
  description = "The size in gigabytes of the additional (non-root) storage device"
  default     = 20000
}

variable "prefix" {
  default = "kinetica"
}

variable "groups" {
  description = "The group to which this instance will belong, to be used for provisioning"
  default     = "kineticadb"
}

resource "aws_instance" "kineticadb" {
  count                       = "${var.instance_count}"
  instance_type               = "${var.instance_type}"
  ami                         = "${var.ami_id}"
  subnet_id                   = "${var.subnet_id}"
  associate_public_ip_address = true
  vpc_security_group_ids      = ["${concat(list(aws_security_group.kineticadb.id), var.vpc_security_group_ids)}"]
  key_name                    = "${aws_key_pair.kineticadb.key_name}"

  root_block_device {
    volume_type           = "${var.storage_type}"
    volume_size           = "${var.storage_size}"
    iops                  = "${var.storage_iops}"
    delete_on_termination = true
  }

  tags {
    Index  = "${count.index + 1}"
    Name   = "${format("%s-gpudb-%1d", var.prefix, count.index + 1)}"
    User   = "${var.ssh_user}"
    Groups = "${var.groups}"
  }

  connection {
    user        = "${var.ssh_user}"
    private_key = "${file(var.ssh_key_file)}"
  }

  provisioner "local-exec" {
    command = "${path.module}/../../scripts/update-ssh-hostkey.sh ${self.public_ip}"
  }
}

output "instance_count" {
  value = "${aws_instance.kineticadb.count}"
}

output "instance_ids" {
  value = "${join(",", aws_instance.kineticadb.*.id)}"
}

output "inventory_names" {
  value = "${join(",", aws_instance.kineticadb.*.tags.Name)}"
}

output "private_ips" {
  value = "${join(",", aws_instance.kineticadb.*.private_ip)}"
}

output "public_ips" {
  value = "${join(",", aws_instance.kineticadb.*.public_ip)}"
}

output "groups" {
  value = "${var.groups}"
}
